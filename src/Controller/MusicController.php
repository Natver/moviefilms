<?php

namespace App\Controller;

use App\Entity\Film;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MusicController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        
        $repo = $this->getDoctrine()->getRepository(Film::class);
        $films = $repo->findAll();
        return $this->render('music/index.html.twig', [
            'controller_name' => 'MusicController',
            'films' => $films
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('music/about.html.twig', [
            'controller_name' => 'MusicController',
        ]);
    }

    /**
     * @Route("/film/{id}", name="film_show")
     */
    public function film_show(Film $film)
    {
        return $this->render('music/film_show.html.twig', [
            'controller_name' => 'MusicController',
            'film' => $film
        ]);
    }

    /**
     * @Route("/film/{id}/edit", name="film_edit")
     * @Route("/films/add", name="film_new")
     */
    public function film_mod(Film $film = null, Request $request, EntityManagerInterface $manager)
    {
        if(!$film){
        $film = new Film();
    }

    $form = $this->createFormBuilder($film)
        ->add('name', TextType::class, [
            'attr' => [
                'placeholder' => 'Nom film'
            ]
        ])
        ->add('description', TextareaType::class, [
            'attr' => [
                'placeholder' => 'Description film'
            ]
        ])
        ->add('image', TextType::class, [
            'attr' => [
                'placeholder' => 'Lien image plat'
            ]
        ])
        ->getForm();
    
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()) {
        $manager->persist($film);
        $manager->flush();

        return $this->redirectToRoute('film_show', ['id' => $film->getId()]);
    }
        return $this->render('music/film_mod.html.twig', [
        'formfilm' => $form->createView(),
        'editMode' => $film->getId() !== null
    ]);
}

    /**
     * @Route("/film/{id}/del", name="film_del")
     */
    public function film_del(Film $film, EntityManagerInterface $manager)
    {
        $manager->remove($film);
        $manager->flush();
        return $this->redirectToRoute('index', [
            'film' => $film
        ]);
    }
}

    
    
