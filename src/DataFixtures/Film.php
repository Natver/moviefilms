<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Film as fmovie;

class Film extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $film = new fmovie();
            $film->setName("film n°$i")
                 ->setDescription("Description du film n°$i")
                 ->setImage("http://placehold.it/200x300");
                 
            $manager->persist($film);
        }

        $manager->flush();
    }
}
